import java.util.Arrays;

public class Main {
    public static void directSelectionSort(char[] array) {
        for (int i = 0; i < array.length; i++) {
            char min = Character.MAX_VALUE;
            int index = i;

            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[i] && array[j] < min) {
                    min = array[j];
                    index = j;
                }
            }

            var temp = array[i];
            array[i] = array[index];
            array[index] = temp;
        }
    }

    public static void bubbleSort(char[] array) {
        boolean wasSwapped;

        do {
            wasSwapped = false;

            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i + 1]) {
                    var temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;

                    wasSwapped = true;
                }
            }
        } while (wasSwapped);
    }

    public static void insertionSort(char[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i; j >= 0; j--) {
                if (array[j + 1] > array[j]) {
                    break;
                }

                var temp = array[j + 1];
                array[j + 1] = array[j];
                array[j] = temp;
            }
        }
    }

    public static void main(String[] args) {
        final char[] originalArray = { 'a', 'd', 'c', 'g', 'b', 'e', 'f', 'h', 'i' };
        System.out.println("Original:\t\t" + Arrays.toString(originalArray));

        char[] rightAnswer = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i' };
        System.out.println("Right answer:\t" + Arrays.toString(rightAnswer));

        var array = originalArray.clone();
        directSelectionSort(array);
        System.out.println(Arrays.toString(array));
        System.out.println(Arrays.equals(array, rightAnswer));

        array = originalArray.clone();
        bubbleSort(array);
        System.out.println(Arrays.toString(array));
        System.out.println(Arrays.equals(array, rightAnswer));

        array = originalArray.clone();
        insertionSort(array);
        System.out.println(Arrays.toString(array));
        System.out.println(Arrays.equals(array, rightAnswer));

        // Testing with the empty array
        array = new char[] {};
        directSelectionSort(array);
        bubbleSort(array);
        insertionSort(array);
    }
}
